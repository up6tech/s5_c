#include <stdio.h>
#include <string.h>

/*
Algorithme: ProduitEntier

Variables:
    Valeur : entier
    Resultat : entier
    EstFini : chaîne de caractères
Debut
    Ecrire("Entrez un nombre")
    Lire(Resultat)
    Faire
        Ecrire("Entrez un nombre")
        Lire(Valeur)
        Resultat <- Resultat * Valeur
        Ecrire("Avez vous terminé ? (y / n)")
        Lire(EstFini)
    TantQue(EstFini != "y")
    Ecrire("Résultat : ", Resultat)
Fin
*/

int main(int argc, char const *argv[])
{
    int nbNombre;
    float valeur, poids, result = 0;
    do
    {
        printf("Combien de nombres voulez vous sommer ? ");
        scanf("%d", &nbNombre);
    } while (nbNombre <= 0);
    for (int i = 0; i < nbNombre; ++i)
    {
        printf("Entrez le nombre à sommer : ");
        scanf("%f", &valeur);
        do
        {
            printf("Entrez le poids du nombre (entre 0 et 1) ");
            scanf("%f", poids);
        } while (poids > 1 || poids < 0);
        result += valeur * poids;
    }
    printf("Résultat : %f\n", result);
}
