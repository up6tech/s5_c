#include <stdio.h>

/*
Algorithme: Parité

Variables:
    Valeur : entier
Debut
    Valeur <- 0
    Ecrire("Veuillez saisir un nombre entier positif")
    Faire
        Lire(Valeur)
        Si(Valeur < 0) Alors
            Ecrire("Veuillez saisir un nombre entier positif")
    TanteQue(Valeur < 0)
    Si(Valeur mod 2 == 0) Alors
        Ecrire("Le nombre est pair")
    Sinon
        Ecrire("Le nombre est impair")
Fin
*/

int main(int argc, char const *argv[])
{
    int valeur;
    printf("Veuillez saisir un nombre entier positif");

    do{
        scanf("%d", &valeur);
        if(valeur < 0){
            printf("Veuillez saisir un nombre entier positif");
        }
    }while(valeur < 0);

    if(valeur % 2 == 0) printf("Le nombre est pair");
                   else printf("Le nombre est impair");

    return 0;
}
