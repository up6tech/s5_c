#include <stdio.h>

/*
Algorithme : Calculette 
Variables:
    Valeur1 : entier
    Valeur2 : entier
Debut
    Valeur1 <- 4
    Valeur2 <- 7
    Ecrire("Somme: ", Valeur1 + Valeur2)
    Ecrire("Produit: ", Valeur1 * Valeur2)
    Ecrire("Soustraction: ", Valeur1 - Valeur2)
    Ecrire("Division entière: ", Valeur1 / Valeur2)
    Ecrire("Reste division entière: ", Valeur1 mod Valeur2)
Fin
*/

int main(int argc, char const *argv[])
{
    int valeur1, valeur2;
    valeur1 = 4;
    valeur2 = 7;

    printf("Somme: %d\n", (valeur1 + valeur2));
    printf("Produit: %d\n", (valeur1 * valeur2));
    printf("Soustraction: %d\n", (valeur1 - valeur2));
    printf("Division entiére: %d\n", (valeur1 / valeur2));
    printf("Reste division entière: %d\n", (valeur1 % valeur2));
    return 0;
}
