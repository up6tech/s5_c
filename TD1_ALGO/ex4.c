#include <stdio.h>
#include <string.h>

/*
Algorithme: ProduitEntier

Variables:
    Valeur : entier
    Resultat : entier
    EstFini : chaîne de caractères
Debut
    Ecrire("Entrez un nombre")
    Lire(Resultat)
    Faire
        Ecrire("Entrez un nombre")
        Lire(Valeur)
        Resultat <- Resultat * Valeur
        Ecrire("Avez vous terminé ? (y / n)")
        Lire(EstFini)
    TantQue(EstFini != "y")
    Ecrire("Résultat : ", Resultat)
Fin
*/

int main(int argc, char const *argv[])
{
    int valeur, resultat;
    char *estFini;
    printf("Entrez un nombre");
    scanf("%d", resultat);
    do
    {
        printf("Entrez un nombre");
        if (scanf("%d", valeur) > 0)
        {
            resultat *= valeur;
            printf("Avez vous terminé ? (y / n)");
            scanf("%s", estFini);
        }
    } while (strcmp(&estFini, "y") != 0);

    printf("Résultat : %d", resultat);

    return 0;
}
