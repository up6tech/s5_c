#include <stdio.h>

/*
Algorithme: Echange

Variables:
    Valeur1 : entier
    Valeur2 : entier
    Tampon: entier
Debut
    Valeur1 <- 5
    Valeur2 <- 7
    Ecrire("Valeur1 : ", Valeur1, " Valeur2 : ", Valeur2)

    Tampon <- Valeur1
    Valeur1 <- Valeur2
    Valeur2 <- Tampon

    Ecrire("Valeur1 : ", Valeur1, " Valeur2 : ", Valeur2)
Fin
*/

int main(int argc, char const *argv[])
{
    int valeur1, valeur2, tampon;
    valeur1 = 5;
    valeur2 = 7;

    printf("Valeur1 : %d, Valeur2 : %d", valeur1, valeur2);

    tampon = valeur1;
    valeur1 = valeur2;
    valeur2 = tampon;

    printf("Valeur1 : %d, Valeur2 : %d", valeur1, valeur2);

    return 0;
}
