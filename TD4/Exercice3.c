
/*********************
 *   Dorian Gardes   *
 *      STRI1A C     *
 *    20/10/2020     *
 *********************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define TAILLE_NOM 20
#define TAILLE_PRENOM 20

/* Définition des types personnalisés */

typedef struct liste {
   struct etudiant* premier;
   struct etudiant* dernier;
} liste;

typedef struct etudiant {

    struct etudiant* precedent;
    struct etudiant* suivant;

    char prenom[TAILLE_PRENOM];
    char nom[TAILLE_NOM];
    int age;

} etudiant;

/* Méthodes pour simplifier la manipulation de la liste chainée */

bool ajouterListe(struct liste* listeEtudiants) {

    char nom[TAILLE_NOM];
    printf("Entrez un nom (tapez \"fin\" pour terminer) :\n");
    fflush(stdin);
    fgets(nom, TAILLE_NOM, stdin);
    nom[strlen(nom) - 1] = 0; // On retire le '\n' de fin de chaine

    if (strcmp(nom, "fin") == 0) return false; // On demande la fin donc on sors
    else {

        struct etudiant* current = malloc(sizeof(etudiant));
        current->suivant = NULL;
        strcpy((*current).nom, nom);
        printf("Entrez un prénom :\n");
        fflush(stdin);
        fgets(current->prenom, TAILLE_PRENOM, stdin);
        current->prenom[strlen(current->prenom) - 1] = 0; // On retire le '\n' de fin de chaine
        do {
            printf("Entrez l'age de %s %s :\n", current->prenom, current->nom);
            fflush(stdin);
        } while(scanf("%d", &(current->age)) < 1);

        //La structure est prête on l'insère dans la liste
        if (listeEtudiants->dernier != NULL) {
            listeEtudiants->dernier->suivant = current;
            current->precedent = listeEtudiants->dernier;
            listeEtudiants->dernier = current;

        } else { //La chaine est vide donc on la crée
            current->precedent = NULL;
            listeEtudiants->premier = current;
            listeEtudiants->dernier = current;
        }
        return true;
    }
}

void retirerListe(struct liste* listeEtudiants, struct etudiant* aRetirer) {

    if (aRetirer == listeEtudiants->premier) {
        listeEtudiants->premier = aRetirer->suivant;
        free(aRetirer);
    } else if (aRetirer == listeEtudiants->dernier) {
        listeEtudiants->dernier = aRetirer->precedent;
        free(aRetirer);
    } else { // On retire une personne au coeur de la liste
        aRetirer->suivant->precedent = aRetirer->precedent;
        aRetirer->precedent->suivant = aRetirer->suivant;
        free(aRetirer);
    }
}

void etudiantToString(struct etudiant* etud) {
    printf("%s %s, %d ans\n", etud->prenom, etud->nom, etud->age);
}

/* Fonction MAIN */
int main() {
    setbuf(stdout, 0);
    struct liste* listeEtudiants = malloc(sizeof(liste));

    listeEtudiants->premier = NULL;
    listeEtudiants->dernier = NULL;

    while (ajouterListe(listeEtudiants)) {}

    printf("Affichage de la liste des étudiants\n");
    struct etudiant* current = listeEtudiants->premier;
    int nbrEtudiants = 0;
    while (current != NULL) {
        etudiantToString(current);
        current = current->suivant;
        nbrEtudiants ++;
    }
    printf("Affichage des %d étudiants terminée\n", nbrEtudiants);

    while (listeEtudiants->premier != NULL) { retirerListe(listeEtudiants, listeEtudiants->premier); }

    return 0;
}
