
/*********************
 *   Dorian Gardes   *
 *      STRI1A C     *
 *    20/10/2020     *
 *********************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

unsigned int lireStdin(char** p) {

    fgets(*p, INT_MAX, stdin);
    return strlen(*p);
}

int main() {
    setbuf(stdout, 0);

    char* p;
    printf("Ecrivez quelque chose :\n");
    unsigned int len = lireStdin(&p);

    if (len >= 0) printf("Vous avez tapé un message de %d charactères :\n%s", len, p);
    else printf("Il y a eu une erreur lors de la saisie.\n");

    free(p);
    return 0;
}
