
/*********************
 *   Dorian Gardes   *
 *      STRI1A C     *
 *    20/10/2020     *
 *********************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void afficherTexte(char** texte) {
    for (int i = 0; i < 10; ++i) {
        printf("%s", texte[i]);
    }
    fflush(stdout); // On force le vidage du buffer
}

int main() {
    char lecture[500];
    char* texte[10];

    for (int i = 0; i < 10; ++i) {
        int nbrCharLu = 0;
        do {
            printf("Entrez la phrase n°%d\n", i+1);
            fgets(lecture, 500, stdin);
        } while ( strlen(lecture) < 1);

        texte[i] = calloc(strlen(lecture) +1 , sizeof(char)); // On ajoute 1 pour '\0'
        if (texte[i] == NULL) {
            printf("ERREUR D'ALLOCATION DYNAMIQUE\nABANDON");
            exit(EXIT_FAILURE);
        }
        strcpy(texte[i], lecture);
    }
    afficherTexte(&texte);
    for (int i = 0; i < 5; ++i) {
        char* temp;
        temp = texte[i];
        texte[i] = texte[9-i];
        texte[9-i] = temp;
    }
    afficherTexte(&texte);
    for (int i = 0; i < 10; ++i) {
        free(texte[i]);
    }
    return 0;
}
