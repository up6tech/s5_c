#include <stdio.h>
#include <errno.h>
#include <string.h>

extern int errno;

int main(int argc, char const *argv[]) {

    if (argc != 3) {
        printf("Ce programme prend le chemin de deux fichiers en paramètre: fichierSource fichierCible\n");
        return 0;
    }

    FILE *fileIn = NULL;
    fileIn = fopen(argv[1], "r");
    FILE *fileOut = NULL;
    fileOut = fopen(argv[2], "w");

    if (fileIn == NULL || fileOut == NULL) {
        if (fileIn == NULL) {
            printf("ERREUR Impossible d'ouvrir le fichier source : %s\n", strerror(errno));
            fclose(fileOut);
        } else {
            printf("ERREUR Impossible d'ouvrir/créer le fichier cible : %s\n", strerror(errno));
            fclose(fileIn);
        }

    } else {
        char currentChar;

        while (feof(fileIn) == 0) {
            currentChar = fgetc(fileIn);
            if (currentChar == EOF) break; // just in case
            fputc(currentChar, fileOut);
        }
        fclose(fileIn);
        fclose(fileOut);
    }
    return 0;
}
