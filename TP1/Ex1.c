#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    // Variables
    char cNom[80], cArticle[80];
    int iNombre, iPrix;
    FILE* file;

    // Ouverture du fichier en mode read-only
    file = fopen("./FichierTexte1.txt", "r");

    // Tant qu'on a pas atteint la fin du fichier (EOF = end of file)
    // On affecte les données de la ligne aux variables respectives
    while(fscanf(file, "%s %s %d %d\n", cNom, cArticle, &iNombre, &iPrix) != EOF){
        // On affiche à l'utilisateur les variables
        printf("La personne %s à acheté %d %s au prix de %d€\n", cNom, iNombre, cArticle, iPrix);
    }

    // On ferme le fichier
    fclose(file);
    return 0;
}
