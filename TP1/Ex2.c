#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    
    int iNbPersonne = 0;
    char cNomPersonne[80];
    char cNomFichier[80];

    // Ecriture fichier
    //   On demande le nom du fichier à l'utilisateur
    printf("Veuillez saisir le nom du fichier\n");
    if(scanf("%s", cNomFichier) == EOF){
        printf("Erreur nom du fichier invalide");
        exit(1);
    }
    //   On demande le nombre de nom à l'utilisateur
    printf("Veuillez rentrer le nombre de personne à insérer\n");
    if(scanf("%d", &iNbPersonne) == EOF){
        printf("Erreur nombre de personne invalide");
        exit(1);
    }

    // On créer le fichier, en mode a+ (append & le fichier est créé s'il n'existe pas)
    FILE* file;
    file = fopen(cNomFichier, "a+");

    //   On lui demande de saisirs tous les noms
    int i ;
    for(i = 0; i < iNbPersonne; i++){
        printf("Veuillez saisir le nom de la personne n°%d\n", (i+1));
        if(scanf("%s", cNomPersonne) == EOF){
            printf("Erreur nom de personne invalide");
            exit(1);
        }
        //   On écrit dans le fichier
        fprintf(file, "%s\n", cNomPersonne);
    }

    fclose(file);

    // Lecture fichier
    //   Ouverture du fichier en read-only
    file = fopen(cNomFichier, "r");

    char line[100];
    const int MaxLen = 100;
    char *pLine;
    int iLigneCourrante = 0;

    while( (pLine = fgets(line, MaxLen, file)) != NULL){
        iLigneCourrante++;
        // Lecture du nom de a personne (i-ème ligne)
        printf("Nom de la personne n°%d : %s", iLigneCourrante, line);
    }

    // Fermeture du fichier
    fclose(file);

    return 0;
}
