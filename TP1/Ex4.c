#include <stdio.h>
#include <errno.h>
#include <string.h>

extern int errno;

int main(int argc, char const *argv[]) {

    if (argc != 2) {
        printf("Ce programme prend le chemin d'un fichier en paramètre\n");
        return 0;
    }
    /*printf("DEBUG Path : %s\n", argv[0]);
    printf("DEBUG Fichier : %s\n", argv[1]);*/

    FILE* file = NULL;
    file = fopen(argv[1], "r");

    if (file == NULL) {
        printf("ERREUR Impossible d'ouvrir le fichier : %s\n", strerror( errno ));
    } else {
        fseek(file, 0, SEEK_END);
        printf("Le fichier fait %ld octets\n", ftell(file));
        fclose(file);
    }
    return 0;
}
