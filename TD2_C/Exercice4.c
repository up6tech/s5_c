#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char *CH = (char *)malloc(20 * sizeof(char));
    printf("Veuillez rentrer une chaîne de caractère\n");
    fgets(CH, 20, stdin);
    printf(CH);
    size_t taille = strlen(CH);
    int i, dernierCar;

    for (dernierCar = 0; CH[dernierCar]; dernierCar++)
        ;
    dernierCar -= 2;
    for (i = 0; i < dernierCar; i++, dernierCar--){
        char at = CH[i];
        char rev = CH[dernierCar];
        if (at != rev){
            i = -1;
            break;
        }
    }
    if (i == -1){
        printf("Ce n'est pas un palindrome\n");
    }else{
        printf("C'est un palindrome\n");
    }
    free(CH);
    return 0;
}