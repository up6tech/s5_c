#include <stdio.h>
#include <stdlib.h>

void echanger(float* a, float* b){
    float temp = *a;
    *a = *b;
    *b = temp;
}

int main(int argc, char const *argv[])
{
    float val1, val2;

    printf("Veuillez saisir 2 nombre\n");
    if(scanf("%f", &val1) == 0){
        printf("Ce n'est pas un nombre.\n");
        exit(1);
    }
    fflush(stdin);
    if(scanf("%f", &val2) == 0){
        printf("Ce n'est pas un nombre.\n");
        exit(1);
    }
    printf("Vous avez choisi %f et %f\n", val1, val2);
    echanger(&val1, &val2);
    printf("Apres echangement, les valeurs sont %f et %f", val1, val2);
    return 0;
}
