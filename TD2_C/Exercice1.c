#include <stdio.h>

int main(int argc, char const *argv[])
{
    
    int i;
    int* p;
    i = 5;
    p = &i;

    printf("Valeur de i %d\n", i);

    *p = 3;

    printf("Valeur de i %d\n", i);

    return 0;
}
