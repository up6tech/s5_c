#include <stdio.h>
#include <stdlib.h>

struct coord
{
  int x;
  int y;
};

struct rectangle
{
  struct coord* topleft;
  struct coord* botright;
};

struct coord* askCoord()
{
  struct coord *coordonee = malloc(1 * sizeof(struct coord));
  printf("X ? \n");
  if (scanf("%d", &coordonee->x) == 0)
  {
    exit(0);
  }
  printf("Y ? \n");
  if (scanf("%d", &coordonee->y) == 0)
  {
    exit(0);
  }
  return coordonee;
}

int main(int argc, char const *argv[])
{
  struct rectangle* rect = malloc(1 * sizeof(struct rectangle));
  printf("Veuillez rentrer les coordonées du coin supérieur gauche\n");
  rect->topleft = askCoord();
  printf("Veuillez rentrer les coordonées du coin inférieur droit\n");
  rect->botright = askCoord();

  int longeur = rect->botright->x - rect->topleft->x;
  int largeur = rect->topleft->y - rect->botright->y;
  int aire = longeur * largeur;
  printf("L'aire est de %dm²\n", aire);

  free(rect->topleft);
  free(rect->botright);
  free(rect);
  return 0;
}
