#include <stdio.h>
#include <stdlib.h>

void printarray(int array[], int size)
{
  printf("[");
  for (int i = 0; i < size; i++)
  {
    printf("%d", array[i]);
    if (i != size - 1)
    {
      printf(",");
    }
    else
    {
      printf("]\n");
    }
  }
}

int main(int argc, char const *argv[])
{
  int arrayLength;
  printf("Veuillez rentrer la taille du tableau\n");
  if (scanf("%d", &arrayLength) == 0)
  {
    printf("Ce n'est pas un nombre");
    exit(0);
  }
  if (arrayLength > 50)
  {
    printf("La taille maximale est de 50");
  }
  printf("La taille du tableau est %d\n", arrayLength);
  int i = 0;
  int array[arrayLength];

  for (i = 0; i < arrayLength; i++)
  {
    int isCorrect = 0;
    int data;
    do
    {
      printf("Veuillez saisir un nombre pour l'index %d\n", i);
      isCorrect = scanf("%d", &data);
    } while (isCorrect == 0);

    array[i] = data;
  }

  printf("Tableau : \n");
  printarray(array, arrayLength);

  // Inversion du tableau
  for (i = 0; i < arrayLength / 2; i++)
  {
    int temp = array[i];
    array[i] = array[arrayLength - i - 1];
    array[arrayLength - i - 1] = temp;
  }
  printf("Tableau inversé\n");
  printarray(array, arrayLength);

  return 0;
}