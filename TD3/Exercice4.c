#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef union 
{
  unsigned int entier;
  unsigned char donnee[4];
} conversion;

int main(int argc, char const *argv[])
{
  conversion adr_ip, s_masque, adr_reseau, adr_machine;
  

  printf("Veuillez saisir l'adresse IP de la machine\n");
  scanf("%hhu.%hhu.%hhu.%hhu", &adr_ip.donnee[0],
                         &adr_ip.donnee[1],
                         &adr_ip.donnee[2],
                         &adr_ip.donnee[3]);

  printf("Veuillez saisir le masque de réseau\n");
  scanf("%hhu.%hhu.%hhu.%hhu", &s_masque.donnee[0],
                         &s_masque.donnee[1],
                         &s_masque.donnee[2],
                         &s_masque.donnee[3]);
  
  adr_reseau.entier = adr_ip.entier & s_masque.entier;
  adr_machine.entier = adr_ip.entier & ~s_masque.entier;

  printf("Adresse réseau : %hhu.%hhu.%hhu.%hhu\n", 
                  adr_reseau.donnee[0],
                  adr_reseau.donnee[1],
                  adr_reseau.donnee[2],
                  adr_reseau.donnee[3]);

  printf("Adresse machine : %hhu.%hhu.%hhu.%hhu\n", 
                  adr_machine.donnee[0],
                  adr_machine.donnee[1],
                  adr_machine.donnee[2],
                  adr_machine.donnee[3]);

  return 0;
}
