#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct livre
{
  char titre[20];
  float prix;
};

void removeCRLF(char *content)
{
  content[strlen(content) - 1] = 0;
}

int main(int argc, char const *argv[])
{
  struct livre bibliotheque[5];
  int nbLivre = 0;

  printf("Que voulez vous dans votre bibliothèque ?\n");

  // Initialisation du buffer
  char *buffer = (char *)malloc(20 * sizeof(char));
  do
  {
    // On demande le titre à l'utilisateur
    printf("Saisissez un titre de livre OU # pour arréter le processus\n");
    fgets(buffer, 20, stdin);
    // On supprime le \n
    removeCRLF(buffer);
    // Si l'utilisateur veut arrêter
    if (strcmp(buffer, "#") == 0)
    {
      break;
    }
    fflush(stdin);
    printf("Saissez le prix du livre %s\n", buffer);
    // On transfère le titre contenu dans le buffer vers le titre du livre
    // On transfère le titre
    strcpy(bibliotheque[nbLivre].titre, buffer);

    fgets(buffer, 20, stdin);
    // On supprime le \n
    removeCRLF(buffer);

    fflush(stdin);
    if (strcmp(buffer, "#") == 0)
    {
      break;
    }

    float prix = 0.0f;
    sscanf(buffer, "%f", &prix);
    bibliotheque[nbLivre].prix = prix;
    nbLivre++;
  } while (nbLivre < 4);

  float somme = 0;
  for(int i = 0; i < nbLivre; i++){
    somme += bibliotheque[i].prix;
  }

  printf("Le prix total des livres de la bibliothèque = %.2f€\n", somme);

  return 0;
}
